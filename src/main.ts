const ramda = require("ramda");
const jquery = require("jquery");

const { match, curry, map, compose, prop } = ramda;

const head = <T>(xs: T[]) => Maybe.of(xs[0]);
const safeProp = curry((p: string, x: any) => Maybe.of(prop(p)(x)));

class Maybe {
  readonly $value: any;

  static of(x: any) {
    return new Maybe(x);
  }

  get isNothing() {
    return this.$value === null || this.$value === undefined;
  }

  constructor(x: any) {
    this.$value = x;
  }

  public map<T, P>(f: (x: T) => P) {
    return this.isNothing ? this : Maybe.of(f(this.$value));
  }

  public inspect() {
    return this.$value ? "Nothing" : `Just ${this.$value}`;
  }
}

const streetName = compose(map(prop('street')), head, safeProp('address'));
const names = streetName({});

console.log(names);
console.log(Maybe.of("Malkovich Malkovich").map(match(/a/gi)));
