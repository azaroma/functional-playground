const Koa = require("koa");
const serve = require("koa-static");

const app = new Koa();

app.use(serve(__dirname + "/src/public"));

app.listen(3001);
